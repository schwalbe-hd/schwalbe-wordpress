# Schwalbe for WordPress

<div align="center">

<img src="./logo.svg" width="100px" /></a>

</div>

Maintainer: Jakob Moser <moser@cl.uni-heidelberg.de>

Schwalbe for WordPress is a WordPress plugin that allows to display election data concering the Verfasste Studierendenschaft of the Universität Heidelberg on a WordPress page or post.

[[_TOC_]]

## :inbox_tray: Download

[**Download Schwalbe for WordPress**](https://gitlab.com/schwalbe-hd/schwalbe-wordpress/-/archive/master/schwalbe-wordpress-master.zip)

After downloading, go to the `/wp-admin/plugin-install.php` page of your WordPress installation, click on "Upload plugin" and uploda the ZIP file. Don't forget to activate the plugin once you are ready.

## :sparkles: Features

### List office holders for a council partition

You can use the shortcode `[schwalbe_office_holders]` to list all current office holders on a council partition:

```
[schwalbe_office_holders council_partition_uuid="cb494559-235a-4991-b413-dfcacd676a97" show_only_first_name="yes" order_by="name"]
```

The shortcode has the following attributes:

* `council_partition_uuid` *(required)*: The uuid of the council partition whose office holders to display.
  * You can find the uuid on the [councils page in the Schwalbe interface](https://schwalbe.stura.uni-heidelberg.de/councils).
* `show_only_first_name`: Whether only the first names of the office holders should be shown.
  * Accepted values: `"no"` *(default)*, `"yes"`
* `order_by`: How the office holder list should be ordered
  * Accepted values
    * `"name"` *(default)*: Order by first name, A-Z
    * `"votes"`: Order by number of received votes, descendingly
    * `"registered_on"`: Order by date when the candidacy was registered, ascendingly

Only `council_partition_uuid` is required, so the minimal way to embed a list of office holders would be:

```
[schwalbe_office_holders council_partition_uuid="cb494559-235a-4991-b413-dfcacd676a97"]
```

### Show current legislative period

You can use the shortcode `[schwalbe_legislative_period]` to show the current legislative period of a council partition:

```
[schwalbe_legislative_period council_partition_uuid="cb494559-235a-4991-b413-dfcacd676a97" lang="en" format="semester"]
```

The shortcode has the following attributes:

* `council_partition_uuid` *(required)*: The uuid of the council partition whose office holders to display.
  * You can find the uuid on the [councils page in the Schwalbe interface](https://schwalbe.stura.uni-heidelberg.de/councils).
* `format`: How the legislative period should be displayed.
  * Accepted values
    * `date` *(default)*: Display as date range, e.g. "2024-04-01 to 2025-03-31"
    * `semester`: Display as name of semesters, e.g. "Summer Semester 2024 and Winter Semester 2024/2025"
* `lang`: The language to use (e.g., if "to" or "bis" should be used in a date range)
  * Accepted values: `"de"` *(default)*, `"en"`

Only `council_partition_uuid` is required, so the minimal way to show the legislative period would be:

```
[schwalbe_legislative_period council_partition_uuid="cb494559-235a-4991-b413-dfcacd676a97"]
```

### Purge cache

By default, elections and candidacies are cached (using the WordPress Options named `schwalbe_elections_per_council_partition` and `schwalbe_candidacies_per_election`). To purge the cache (forcing a re-request and therefore higher page load times), embed the following shortcode:

```
[schwalbe_purge_cache]
```

If you want to disable cache completely, just embed this at the beginning of any page using Schwalbe shortcodes. Note: This will increase page load time, in our tests from around 0.3s to around 1.3s.

## :scales: License

This project is licensed under the [MIT license](https://opensource.org/licenses/MIT), for details see the file [`LICENSE`](LICENSE).

This license only applies to the original contributions of the project's authors and not necessarily for used contents from third parties. The file [`LICENSE-3RD-PARTY.md`](LICENSE-3RD-PARTY.md) documents, which (parts of which) files come from third parties and under what license they are used.
