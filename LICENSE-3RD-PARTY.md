# Used third party content

## Material Icons

-   File: `logo.svg`
    -   Derived (with modifications) from icon "poll", URL: https://fonts.google.com/icons?selected=Material%20Icons%3Apoll%3A
    -   Author: Google
    -   License: [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
