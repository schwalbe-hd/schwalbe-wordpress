<?php
namespace Schwalbe;

function activate_plugin() {
    add_option("schwalbe_candidacies_per_election", []);
    add_option("schwalbe_elections_per_council_partition", []);

    if(!wp_next_scheduled("schwalbe_update_cache_hook")) {
        wp_schedule_event(time(), "daily", "schwalbe_update_cache_hook");
    }
}

function deactivate_plugin() {
    $next_update_run = wp_next_scheduled("schwalbe_update_cache_hook");
    wp_unschedule_event($next_update_run, "schwalbe_update_cache_hook");
}

function uninstall_plugin() {
    delete_option("schwalbe_candidacies_per_election");
    delete_option("schwalbe_elections_per_council_partition");
}
