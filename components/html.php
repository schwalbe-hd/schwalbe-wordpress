<?php
namespace Schwalbe;

function generate_name_list($candidacies, $show_only_first_name = false) {
    $result = "<ul>";
    foreach($candidacies as $candidacy) {
        if($show_only_first_name) {
            $name = $candidacy->person->firstName;
        } else {
            $name = $candidacy->person->firstName . " " . $candidacy->person->lastName;
        }

        $result .= "<li>" . esc_html($name) . "</li>";
    }
    $result .= "</ul>";
    return $result;
}
