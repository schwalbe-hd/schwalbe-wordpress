<?php
namespace Schwalbe;

use wp_remote_retrieve_body;
use wp_remote_get;

/**
 * Return all elections known to the Schwalbe API, as array of objects.
 */
function fetch_elections() {
    return json_decode(wp_remote_retrieve_body(wp_remote_get("https://schwalbe.stura.uni-heidelberg.de/api/v2/elections")));
}

function fetch_elections_for($council_partition_uuid) {
    $elections = fetch_elections();
    $relevant_elections = [];

    foreach($elections as $election) {
        if($election->councilPartitionUuid === $council_partition_uuid) {
            $relevant_elections[] = $election;
        }
    }

    return $relevant_elections;
}

function get_elections_for($council_partition_uuid) {
    $elections_per_council_partition = get_option("schwalbe_elections_per_council_partition");

    if(!array_key_exists($council_partition_uuid, $elections_per_council_partition)) {
        return update_elections_for($council_partition_uuid);
    } else {
        return $elections_per_council_partition[$council_partition_uuid];
    }
}

function update_elections_for($council_partition_uuid) {
    $elections_per_council_partition = get_option("schwalbe_elections_per_council_partition");
    $relevant_elections = fetch_elections_for($council_partition_uuid);
    $elections_per_council_partition[$council_partition_uuid] = $relevant_elections;
    update_option("schwalbe_elections_per_council_partition", $elections_per_council_partition);
    return $relevant_elections;
}

/**
 * Return the election for the council partition with the given uuid whose legislative period is currently running.
 * 
 * Returns null, unless there is exactly 1 election that fits this criterion.
 *
 * @param string $council_partition_uuid The uuid of a council partition
 */
function get_election_with_current_legislative_period_for($council_partition_uuid) {
    $today = date("Y-m-d");
    $relevant_elections = get_elections_for($council_partition_uuid);
    $current_elections = [];

    foreach($relevant_elections as $election) {
        if($election->legislativePeriod->start <= $today && $today <= $election->legislativePeriod->end) {
            $current_elections[] = $election;
        }
    }

    if(count($current_elections) === 1) {
        return $current_elections[0];
    } else {
        return null;
    }
}

/**
 * Return all candidacies known to the Schwalbe API, as array of objects.
 */
function fetch_candidacies() {
    return json_decode(wp_remote_retrieve_body(wp_remote_get("https://schwalbe.stura.uni-heidelberg.de/api/v2/candidacies")));
}

function fetch_candidacies_for($election_uuid) {
    $candidacies = fetch_candidacies();
    $relevant_candidacies = [];

    foreach($candidacies as $candidacy) {
        if($candidacy->electionUuid === $election_uuid) {
            $relevant_candidacies[] = $candidacy;
        }
    }

    return $relevant_candidacies;
}

function get_candidacies_for($election_uuid) {
    // Try loading the candidacies from cache
    $candidacies_per_election = get_option("schwalbe_candidacies_per_election");

    if(!array_key_exists($election_uuid, $candidacies_per_election)) {
        // They haven't been cached, apparently. Fetch the candidacies, update the cache and return them.
        return update_candidacies_for($election_uuid);
    } else {
        // Candidacies are cached, return from cache.
        return $candidacies_per_election[$election_uuid];
    }
}

function update_candidacies_for($election_uuid) {
    $candidacies_per_election = get_option("schwalbe_candidacies_per_election");
    $relevant_candidacies = fetch_candidacies_for($election_uuid);
    $candidacies_per_election[$election_uuid] = $relevant_candidacies;
    update_option("schwalbe_candidacies_per_election", $candidacies_per_election);
    return $relevant_candidacies;
}

/**
 * Purges the cache, by setting both relevant cache options to an empty array.
 * This means the next time the candidacies for an election or the elections for a council partition
 * are requested, they need to be fetched from the Schwalbe API.
 */
function purge_cache() {
    update_option("schwalbe_candidacies_per_election", []);
    update_option("schwalbe_elections_per_council_partition", []);
}

/**
 * Update the cache, by:
 * 
 * 1. Remembering all the currently cached council partitions
 * 2. Purging the cache
 * 3. Re-requesting all candidacies for the current legislative period for the remember council partitions
 */
function update_cache() {
    $elections_per_council_partition = get_option("schwalbe_elections_per_council_partition");
    purge_cache();
    foreach($elections_per_council_partition as $council_partition_uuid => $elections) {
        $election = get_election_with_current_legislative_period_for($council_partition_uuid);
        get_candidacies_for($election->uuid);
    }
}
