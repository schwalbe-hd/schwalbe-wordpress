<?php
namespace Schwalbe;

function office_holders_shortcode($attributes = [], $content = null) {
    $validated_attributes = validate_office_holders_shortcode_attributes($attributes);
    $election = get_election_with_current_legislative_period_for($validated_attributes["council_partition_uuid"]);

    $candidacies = get_candidacies_for($election->uuid);
    $relevant_candidacies = [];

    foreach($candidacies as $candidacy) {
        if($candidacy->electionStatus === "elected" || $candidacy->electionStatus === "stepped_in") {
            $relevant_candidacies[] = $candidacy;
        }
    }

    sort_candidacies($relevant_candidacies, $validated_attributes["order_by"], $election);

    return generate_name_list($relevant_candidacies, $validated_attributes["show_only_first_name"] === "yes");
}

function legislative_period_shortcode($attributes = [], $content = null) {
    $validated_attributes = validate_legislative_period_shortcode_attributes($attributes);
    $election = get_election_with_current_legislative_period_for($validated_attributes["council_partition_uuid"]);

    return get_legislative_period($election, $validated_attributes["format"], $validated_attributes["lang"]);
}

function purge_cache_shortcode($attributes = [], $content = null) {
    purge_cache();

    return null;
}

/**
 * Validates the given attributes to a [schwalbe_office_holders] shortcode and returns
 * an array that is guaranteed to be of the following format:
 * 
 * [
 *     "council_partition_uuid" => "<A valid UUID>" | null,
 *     "show_only_first_name" => "yes" | "no",
 *     "order_by" => "votes" | "registered_on" | "name"
 * ]
 */
function validate_office_holders_shortcode_attributes($attributes) {
    $normalized = shortcode_atts(
        [
            "council_partition_uuid" => "",
            "show_only_first_name" => "no",
            "order_by" => "name"
        ],
        $attributes,
        "office_holders"
    );
    
    if(!is_uuid($normalized["council_partition_uuid"])) {
        $normalized["council_partition_uuid"] = null;
    }
    
    if($normalized["show_only_first_name"] !== "yes" && $normalized["show_only_first_name"] !== "no") {
        $normalized["show_only_first_name"] = "no";
    }

    if($normalized["order_by"] !== "votes" && $normalized["order_by"] !== "registered_on" && $normalized["order_by"] !== "name") {
        $normalized["order_by"] = "name";
    }

    return $normalized;
}

/**
 * Validates the given attributes to a [schwalbe_legislative_period] shortcode and returns
 * an array that is guaranteed to be of the following format:
 * 
 * [
 *     "council_partition_uuid" => "<A valid UUID>" | null,
 *     "format" => "semester" | "date"
 *     "lang" => "en" | "de",
 * ]
 */
function validate_legislative_period_shortcode_attributes($attributes) {
    $normalized = shortcode_atts(
        [
            "council_partition_uuid" => "",
            "format" => "date",
            "lang" => "de",
        ],
        $attributes,
        "legislative_period"
    );

    if(!is_uuid($normalized["council_partition_uuid"])) {
        $normalized["council_partition_uuid"] = null;
    }

    if($normalized["format"] !== "date" && $normalized["format"] !== "semester") {
        $normalized["format"] = "date";
    }

    if($normalized["lang"] !== "de" && $normalized["lang"] !== "en") {
        $normalized["lang"] = "de";
    }

    return $normalized;
}
