<?php
/*
 * Plugin Name:     Schwalbe for WordPress
 * Plugin URI:      https://gitlab.com/schwalbe-hd/schwalbe-wordpress
 * Description:     Display election data (e.g., office holders) fetched from schwalbe.stura.uni-heidelberg.de in WordPress.
 * Version:         1.0.2
 * Author:          Jakob Moser
 * License:         MIT
 * License URI:     https://gitlab.com/schwalbe-hd/schwalbe/-/blob/master/LICENSE?ref_type=heads
 */
namespace Schwalbe;

use DateTimeImmutable;

require_once __DIR__ . "/components/api.php";
add_action("schwalbe_update_cache_hook", 'Schwalbe\update_cache');

require_once __DIR__ . "/components/lifecycle.php";
register_activation_hook(__FILE__, 'Schwalbe\activate_plugin');
register_deactivation_hook(__FILE__, 'Schwalbe\deactivate_plugin');
register_uninstall_hook(__FILE__, 'Schwalbe\uninstall_plugin');

require_once __DIR__ . "/components/shortcodes.php";
add_shortcode("schwalbe_office_holders", 'Schwalbe\office_holders_shortcode');
add_shortcode("schwalbe_legislative_period", 'Schwalbe\legislative_period_shortcode');
add_shortcode("schwalbe_purge_cache", 'Schwalbe\purge_cache_shortcode');

require_once __DIR__ . "/components/html.php";

/**
 * Sort the given candidacies by the given criterion ("name", "votes" or "registered_on") in place.
 * $election is needed when sorting by votes.
 */
function sort_candidacies(&$candidacies, $criterion, $election) {
    if($criterion === "name") {
        usort($candidacies, function($a, $b) {
            return strcmp("{$a->person->firstName} {$a->person->lastName}", "{$b->person->firstName} {$b->person->lastName}");
        });
    } else if($criterion === "votes") {
        $candidacies_by_uuid = [];
        foreach($candidacies as $candidacy) {
            $candidacies_by_uuid[$candidacy->uuid] = $candidacy;
        }

        $candidacies = []; // Clear candidacies, rebuild it using $candidacies_by_uuid and $election->results, which are ordered by votes.
        foreach($election->results as $result) {
            $candidacies[] = $candidacies_by_uuid[$result->candidacyUuid];
        }
    } else {
        // This handles the case that $criterion === "registered_on" as well as any invalid criteria.
        // In all those cases, we just leave the list of candidacies as-is, because the API guarantees us
        // that candidacies are sorted by registration timestamp when requesting them without further parameters.
        return;
    }
}

/**
 * Return the legislative period of an election in a human-readable way. Multiple formats are possible:
 * 
 * E.g. given the legislative period starting 2023-04-01 and ending 2024-03-31, the following outputs are possible:
 * 
 * - $format "date", $lang "de": "2023-04-01 bis 2024-03-31"
 * - $format "date", $lang "en": "2023-04-01 to 2024-03-31"
 * - $format "semester", $lang "de": "Sommersemester 2023 und Wintersemester 2023/2024"
 * - $format "semester", $lang "en": "Summer Semester 2023 and Winter Semester 2023/2024"
 */
function get_legislative_period($election, $format, $lang) {
    // Needed for both formats
    $start_date = new DateTimeImmutable($election->legislativePeriod->start);
    $end_date = new DateTimeImmutable($election->legislativePeriod->end);    
    $to = $lang === "de" ? "bis" : "to";
    
    if($format === "semester") {
        if($end_date < $start_date) {
            // Invalid input, should not happen. In this case, we return a human-readable error.
            return $lang === "de" ? "nie" : "never";
        }

        $and = $lang === "de" ? "und" : "and";
        $ws = $lang === "de" ? "Wintersemester" : "Winter Semester";
        $ss = $lang === "de" ? "Sommersemester" : "Summer Semester";

        $start_month_day = $start_date->format("m-d");
        $end_month_day = $end_date->format("m-d");
        
        $start_year = $start_date->format("Y");
        $end_year = $end_date->format("Y");
        
        $start_year_next = $start_year + 1;
        $end_year_prev = $end_year - 1;
        $start_year_and_next = "$start_year/$start_year_next";
        $end_year_and_prev = "$end_year_prev/$end_year";
        
        $year_delta = $end_year - $start_year;

        $naming_rules = [
            ["=04-01", "=03-31", "=1", "$ss $start_year $and $ws $end_year_and_prev"],
            ["=04-01", "=03-31", ">1", "$ss $start_year $to $ws $end_year_and_prev"],
            ["=04-01", "=09-30", "=0", "$ss $start_year"],
            ["=04-01", "=09-30", ">0", "$ss $start_year $to $ss $end_year"],
            ["=10-01", "=03-31", "=1", "$ws $end_year_and_prev"],
            ["=10-01", "=03-31", ">1", "$ws $start_year_and_next $to $ws $end_year_and_prev"],
            ["=10-01", "=09-30", "=1", "$ws $start_year_and_next $and $ss $end_year"],
            ["=10-01", "=09-30", ">1", "$ws $start_year_and_next $to $ss $end_year"],
            // TODO Extend rules
        ];

        foreach($naming_rules as $rule) {
            [$start_month_day_criterion, $end_month_day_criterion, $year_delta_criterion, $name] = $rule;

            if("=$start_month_day" === $start_month_day_criterion) {
                if("=$end_month_day" === $end_month_day_criterion) {
                    if(("=$year_delta" === $year_delta_criterion) || ($year_delta > 0 && $year_delta_criterion === ">0") || ($year_delta > 1 && $year_delta_criterion === ">1")) {
                        return $name;
                    }
                }
            }
        }
    }

    // This is used not only if the $format === "date", but also as graceful fallback if the semester
    // format logic was unable to determine which semesters are specified by the $start to $end date range.
    return "{$start_date->format('Y-m-d')} $to {$end_date->format('Y-m-d')}"; 
}

function is_uuid($string) {
    return preg_match("/^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/", $string);
}
